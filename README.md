AWS ECS GitLab CE
=================
This role is part of the [Mirabeau Cloud Framework](https://gitlab.com/mirabeau/cloud-framework/)

Create GitLab CE ECS services with CloudFormation.

AWS resources that will be created are:
 * RDS PostgreSQL instance
 * TaskDefinitions
   * GitLab CE Web
 * ELB Target group (if target ALB is given)
 * ELB Listener 80 Rule (if target ALB is given)
 * ELB Listener 443 Rule (if target ALB is given AND enable_443 is set to True - requires your ALB to have this listener)
 * Route53 record (if target ALB is given)

Because the RDS will be created with a new user for the database, some manual action is still required. GitLab needs to run `CREATE EXTENSION IF NOT EXISTS "pg_trgm"` to be able to provision the database. The user created does not have the permissions required to create extensions. Therefor you will need to connect to the database yourself as the root user and run it manually before GitLab can be started for the first time.

Requirements
------------
Ansible version 2.5.4 or higher  
Python 2.7.x  
Pip 18.x or higher (Python 2.7)

Required python modules:
* boto
* boto3
* awscli
* docker

The role itself does not create an ECS cluster but takes an existing cluster name as a parameter for deployment.
Also note that while the Listener Rule(s) will be created it does not include any security group ingress rules since these are very customer specific.
You will need to add those yourself - i.e. in the `env-securitygroups-ingress` role.

Dependencies
------------
 * aws-lambda
 * aws-iam
 * aws-vpc
 * aws-vpc-lambda
 * aws-securitygroups
 * aws-hostedzone
 * aws-ecs-cluster
 * aws-ecs-alb

Role Variables
--------------
### _General_
The following params should be available for Ansible during the rollout of this role:
```yaml
aws_region      : <aws region, eg: eu-west-1>
owner           : <owner, eg: mirabeau>
account_name    : <aws account name>
account_abbr    : <aws account generic environment>
environment_type: <environment>
environment_abbr: <environment abbriviation>
```

Role Defaults
-------------
```yaml
create_changeset      : True
debug                 : False
cloudformation_tags   : {}
tag_prefix            : "mcf"

aws_ecs_gitlab_ce_params:
  create_changeset: "{{ create_changeset }}"
  debug           : "{{ debug }}"

  image           : "{{ account_id }}.dkr.ecr.{{ aws_region }}.amazonaws.com/gitlab-ce"
  image_tag       : "10.0.3-ce.0"

  enable_ssh      : False # NOTE: Enabling this disables the use of an ALB since we need a classic ELB for port 22 TCP forwarding....
  elb_scheme      : "internet-facing" # Only used when SSH is enabled
  certificate_arn : "{{ external_ssl_cert | default('') }}"

  alb_role        : "shared-services"
  ecs_cluster_name: "services-ecs"
  desiredcount    : 1

  database:
    name            : "gitlab"
    username        : "gitlab"
    endpoint_address: ""  # Optional, use if DB already exists

  rds:
    username     : "root"
    storage      : 50
    instance_type: "db.t2.small"
    version      : "10.6"
    snapshot_name: ""  # Optional
    multi_az     : False
    encrypt      : True
```

Example Playbooks
-----------------
Build docker image and push to ECR
```yaml
---
- hosts: localhost
  tasks:
    - name: "gitlab | docker | build and push"
      include_role:
        name: aws-ecs-gitlab-ce
        tasks_from: build-docker
      tags:
        - aws:ecs:gitlab
```

Rollout the aws-ecs-gitlab-ce files with defaults
```yaml
- hosts: localhost
  connection: local
  gather_facts: False
  vars:
    tag_prefix      : "mcf"
    aws_region      : "eu-west-1"
    owner           : "myself"
    account_name    : "my-dta"
    account_abbr    : "dta"
    environment_type: "test"
    environment_abbr: "tst"
    internal_route53:
      domainname: my.cloud
    aws_vpc_params:
      network               : "10.10.10.0/24"
      public_subnet_weight  : 1
      private_subnet_weight : 3
      database_subnet_weight: 1
  pre_tasks:
    - name: "Get latest AWS AMI's"
      include_role:
        name: aws-utils
        tasks_from: get_aws_amis
    - name: "Docker build and push"
      include_role:
        name: aws-ecs-gitlab-ce
        tasks_from: build-docker
  roles:
    - aws-setup
    - aws-iam
    - aws-vpc
    - env-acl
    - aws-vpc-lambda
    - aws-securitygroups
    - aws-lambda
    - aws-ecs-cluster
    - aws-ecs-alb
    - aws-ecs-gitlab-ce
```
Troubleshooting
------------
Starting from Gitlab 8.6, Gitlab requires the PostgresSQL extension pg_trgm. Since this role uses RDS (an external) database we need to enable it manually.
See below the steps required:

On existing RDS:
```
CREATE DATABASE gitlab;
```
On init:
```
CREATE ROLE gitlab;
GRANT ALL PRIVILEGES ON database gitlab TO gitlab;
CREATE EXTENSION IF NOT EXISTS "pg_trgm";
ALTER ROLE gitlab WITH LOGIN;
ALTER ROLE gitlab WITH PASSWORD '<password>';
ALTER DATABASE gitlab OWNER TO gitlab;
```
If you have the problem that Gitlab doesn't create the db tables properly, you can also do it manually with "gitlab-rake db:migrate"

Source: https://docs.gitlab.com/omnibus/common_installation_problems/#extension-missing-pg_trgm

License
-------
GPLv3

Author Information
------------------
Lotte-Sara Laan <llaan@mirabeau.nl>  
Wouter de Geus <wdegeus@mirabeau.nl>  
Rob Reus <rreus@mirabeau.nl>
